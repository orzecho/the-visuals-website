import { createWebHistory, createRouter } from "vue-router";
import MainPage from "../components/MainPage.vue";
import PrzemianaPage from "../components/PrzemianaPage.vue";
import OpiekunPage from "../components/OpiekunPage.vue";
import LustroPage from "../components/LustroPage.vue";
import * as $ from "jquery";

const routes = [
    {
        path: "/",
        name: "Home",
        component: MainPage,
    },
    {
        path: "/pomyslny-wiatr",
        name: "Pomyślny Wiatr",
        component: OpiekunPage,
    },
    {
        path: "/przemiana",
        name: "Przemiana",
        component: PrzemianaPage,
    },
    {
        path: "/lustro",
        name: "Lustro",
        component: LustroPage,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

router.beforeEach((to, from, next) => {
    $(document).scrollLeft(0);
    next();
});

export default router;
